import sys

slownik_klas = {}
slownik_osob = {}

arguments = sys.argv[1:]
print(arguments)

class Wychowawca:

        def __init__(self, name, classes):
            self.name = name
            self.classes = classes

        def __str__(self):
            return f'Imie nazwisko: {self.name}, Klasy: {self.classes}'

        @classmethod
        def from_file(cls):
            classes = []
            first_name = arguments.pop(0)
            last_name = arguments.pop(0)
            name = f'{first_name}  {last_name}'

            while True:
                if arguments:
                    classes.append(arguments.pop(0))
                else:
                    break
            return cls(name, classes)

def main():
    mentor = Wychowawca.from_file()
    print(mentor)

if __name__ == "__main__":
    main()


class Nauczyciel:
    def __init__(self, name, przedmiot, klasa):
        self.name = name
        self.przedmiot = arguments.pop(0)
        self.klasa = self.pobieranie_klasy()

    def __str__(self):
        return f"Imie nazwisko: {self.name}, Przedmiot: {self.przedmiot}, Klasa: {self.klasa}"

    def from_file(cls):
        classes = []
        first_name = arguments.pop(0)
        last_name = arguments.pop(0)
        name = f'{first_name}  {last_name}'

        while True:
            if arguments:
                classes.append(arguments.pop(0))
            else:
                break
        return cls(name, classes)

    def pobieranie_klasy(self):
        while True:
            klasa = arguments.pop(0)
            if klasa == "":
                break
            lista_klas.append(klasa)
        return klasa


class Klasa:
    def __init__(self, symbol):
        self.wychowawca = imie_nazwisko
        self.uczniowie = []
        self.symbol = symbol

    def __str__(self):
        print(f"Klasa: {self.symbol}, Wychowawca: {self.wychowawca.imie_nazwisko}, Uczniowie: {self.lista_uczniow()}")

    def lista_uczniow(self):
        imiona = []
        for uczen in self.uczniowie:
            imiona.append(uczen.imie_nazwisko)
        return imiona

class Uczen:
    def __init__(self, imie_nazwisko, klasa=""):
        self.name = imie_nazwisko
        self.classes = classes

    def __str__(self):
        return f"Imie nazwisko: {self.name}, Klasa: {self.classes}"

    def from_file(cls):
        first_name = arguments.pop(0)
        last_name = arguments.pop(0)
        name = f"{first_name}  {last_name}"

    def pobierz(self):
        self.imie_nazwisko = self.nazwisko()
        symbol_klasy = arguments.pop(0)

        if symbol_klasy not in slownik_klas:
            self.klasa = Klasa(symbol_klasy)
            slownik_klas[symbol_klasy] = self.klasa
        else:
            self.klasa = slownik_klas[symbol_klasy]
        self.klasa.uczniowie.append(self)

