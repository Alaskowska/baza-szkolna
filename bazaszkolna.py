import sys

x = 1
y = 0

slownik_klas = {}
slownik_osob = {}
lista_klas = []

arguments = sys.argv[1:]
print(arguments)

class Klasa:
    def __init__(self, symbol):
        self.wychowawca = imie_nazwisko
        self.uczniowie = []
        self.symbol = symbol

    def show(self):
        print("Klasa: {}, Wychowawca: {}, Uczniowie: {}".format(
            self.symbol,
            self.wychowawca.imie_nazwisko,
            self.lista_uczniow()))

    def lista_uczniow(self):
        imiona = []
        for uczen in self.uczniowie:
            imiona.append(uczen.imie_nazwisko)
        return imiona

class Uczen:
    def __init__(self, imie_nazwisko, klasa=""):
        self.imie_nazwisko = imie_nazwisko
        self.klasa = klasa

    def nazwisko(self):
        imie = arguments.pop(0)
        nazwisko = arguments.pop(0)
        return "{} {}".format(imie, nazwisko)

    def pobierz(self):
        self.imie_nazwisko = self.nazwisko()
        symbol_klasy = arguments.pop(0)

        if symbol_klasy not in slownik_klas:
            self.klasa = Klasa(symbol_klasy)
            slownik_klas[symbol_klasy] = self.klasa
        else:
            self.klasa = slownik_klas[symbol_klasy]
        self.klasa.uczniowie.append(self)

class Nauczyciel:
    def pobierz(self):
        self.imie_nazwisko = self.nazwisko()
        self.przedmiot = arguments.pop(0)
        self.klasa = self.pobieranie_klasy()

    def nazwisko(self):
        imie = arguments.pop(0)
        nazwisko = arguments.pop(0)
        return "{} {}".format(imie, nazwisko)

    def pobieranie_klasy(self):
        while True:
            klasa = arguments.pop(0)
            if klasa == "":
                break
            lista_klas.append(klasa)
        return klasa

class Wychowawca:
    def pobierz(self):
        self.imie_nazwisko = self.nazwisko()
        self.klasa = self.pobieranie_klasy()

    def nazwisko(self):
        imie = arguments.pop(0)
        nazwisko = arguments.pop(0)
        return "{} {}".format(imie, nazwisko)

    def show(self):
        print("Wychowawca: {}, Klasa: {}".format(
            self.imie_nazwisko,
            self.klasa,
            ))

    def pobieranie_klasy(self):
        wych_klasy = []
        while True:
            if arguments:
                klasa = arguments.pop(0)
            else:
                break
            lista_klas.append(klasa)
            wych_klasy.append(klasa)
        return wych_klasy

while True:

    if not arguments:
        break

    x = arguments.pop(0)
    if x == "wychowawca":
        osoba = Wychowawca()
    if x == "nauczyciel":
        osoba = Nauczyciel()
    if x == "uczen":
        osoba = Uczen()
    if x == "koniec":
        break

    osoba.pobierz()
    print(osoba.imie_nazwisko, osoba.klasa)
    if osoba.imie_nazwisko not in slownik_osob:
            slownik_osob[osoba.imie_nazwisko] = []
    slownik_osob[osoba.imie_nazwisko].append(osoba)

for imie_nazwisko, osoby in slownik_osob.items():
    for osoba in osoby:
        print(osoba.imie_nazwisko)
        osoba.show()

